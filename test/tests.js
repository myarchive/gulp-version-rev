const versionRev = require('../index');
const expect = require('chai').expect;
const gulp = require('gulp');
const path = require('path');

describe('gulp-version-rev', function () {
  it('.html', function (done) {

    let pipe = gulp.src(path.join(__dirname, 'fixture/test.html')).pipe(versionRev({version: '0.0.1'}));
    let resultFile;

    pipe.on('data', function (file) {
      resultFile = file;
    });

    pipe.on('end', function (data) {
      check();
      done();
    });

    function check() {
      let result = resultFile.contents.toString();
      expect(result.match(/\?version=0\.0\.1/g)).to.have.length(3);
    }
  });

  it('.ftl', function (done) {

    let pipe = gulp.src(path.join(__dirname, 'fixture/test.ftl')).pipe(versionRev({
      version: '0.0.1',
      ignoreUrls: [/.+\.png$/g, '${Global.getConfig("context.url")}/js/abc.js']
    }));
    let resultFile;

    pipe.on('data', function (file) {
      resultFile = file;
    });

    pipe.on('end', function (data) {
      check();
      done();
    });

    function check() {
      let result = resultFile.contents.toString();
      expect(result.match(/\?version=0\.0\.1/g)).to.have.length(1);
    }
  });
});
